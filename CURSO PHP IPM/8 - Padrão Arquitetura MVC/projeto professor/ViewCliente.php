<?php
class ViewCliente {

    private $listaClientes = Array();
    
    //HTML do Formul�rio
    public function montaFormulario(){
?>
        <h2>Clientes</h2>
        <hr>
        <?php $this->montaConsulta(); ?>
        <hr>
        <form action="?pagina=clientes" method="POST">
            <div class="field">
                <label id="codigo" class="label_formulario">C�digo:</label>
                <input type="text" id="codigo" name="codigo" size="5"/>
            </div>
            <div class="field">
                <label id="nome" class="label_formulario">Nome:</label>
                <input type="text" id="nome" name="nome" size="50"/>
            </div>
            <div class="field">
                <label id="valorcredito" class="label_formulario">Valor Cr�dito:</label>
                <input type="text" id="valorcredito" name="valorcredito" size="20"/>
            </div>
            <hr>
            <input class="button" type="submit" value="Gravar"/>
            <input class="button" type="reset" value="Limpar"/>
        </form>
<?php   
    }
    
    //HTML da table de lista, percorrendo a lista de clientes
    public function montaConsulta(){
        $sHTML  = '';        
        $sHTML .= '<table class="tabela_consulta"><tr><th>C�digo</th><th>Nome</th><th>Cr�dito</th></tr>';
        foreach ($this->listaClientes as $indice => /* @var $oModelCliente ModelCliente */ $oModelCliente){
            $sHTML.= '<tr>';        
            $sHTML.=   '<td>'.$oModelCliente->getCodigo().'</td>';
            $sHTML.=   '<td>'.$oModelCliente->getNome().'</td>';
            $sHTML.=   '<td>R$ '.$oModelCliente->getValorCredito().'</td>';            
            $sHTML.= '</tr>';        
        }
        $sHTML     .= '</table>';
        echo $sHTML;        
    }
    
    function setModelCliente($ModelCliente) {
        $this->ModelCliente = $ModelCliente;
    }

    function setListaClientes($listaClientes) {
        $this->listaClientes = $listaClientes;
    }
}
