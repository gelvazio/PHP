<?php

class Redirecionador extends RedirecionadorPadrao {

    static $oParametros = Array();

    public function __construct() {
        $this->getAllParametros();
        $this->verificaDestino();
    }

    private function verificaDestino() {
        $oControllerHome = new ControllerHome();
        if (self::getParametro('pagina')) {
            $sController = 'Controller' . self::getParametro('pagina');
            //echo 'Controller Antes de montar:'.$sController.'<br>';
            $sCaminhoController = $this->montaCaminhoArquivo($sController);
            $NomeController = $this->montaNomeArquivo($sController);
            //if (file_exists('./controller/' . $sController . '.php')) {
            if (file_exists($sCaminhoController)) {
                echo 'ENTROU AQUI <br>', $NomeController;

                $oController = new $sController();
                $oController->processaDados();
            } else {
                echo 'Controller:' . $sController . '<br>';
                throw new Exception('P�gina solicitada n�o encontrada! ' . $sController);
            }
        } else if (file_exists('./include/glw/controller/glw_class_controller_home.php')) {
            $oControllerHome->processaDados();
        } else {
            throw new Exception('P�gina home n�o encontrada! ' . $oControllerHome);
        }
    }

    private function getAllParametros() {
        foreach ($_GET as $chave => $valor) {
            self::$oParametros[$chave] = $valor;
        }
        foreach ($_POST as $chave => $valor) {
            self::$oParametros[$chave] = $valor;
        }
    }

    public static function getParametro($name) {
        if (isset(self::$oParametros[$name])) {
            return self::$oParametros[$name];
        }
        return false;
    }

}
