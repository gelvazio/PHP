<?php

class ViewCampo {

    private $CampoNome;
    private $CampoTitulo;
    private $CampoTamanho;
    private $CampoTipo;

    function getCampoNome() {
        return $this->CampoNome;
    }

    function getCampoTitulo() {
        return $this->CampoTitulo;
    }

    function getCampoTamanho() {
        return $this->CampoTamanho;
    }

    function getCampoTipo() {
        return $this->CampoTipo;
    }

    function setCampoNome($CampoNome) {
        $this->CampoNome = $CampoNome;
    }

    function setCampoTitulo($CampoTitulo) {
        $this->CampoTitulo = $CampoTitulo;
    }

    function setCampoTamanho($CampoTamanho) {
        $this->CampoTamanho = $CampoTamanho;
    }

    function setCampoTipo($CampoTipo) {
        $this->CampoTipo = $CampoTipo;
    }

}
