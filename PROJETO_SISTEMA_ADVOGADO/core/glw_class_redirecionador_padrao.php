<?php

class RedirecionadorPadrao {

    function montaCaminhoArquivo($className) {
        $sModulo = 'glw';
        $sInclude = 'include';
        $sController = 'controller';
        $sModel = 'model';
        $sPersistencia = 'persistencia';
        $sView = 'view';

        $sCaminhoController = $sInclude . '/' . $sModulo . '/' . $sController . '/';
        $sCaminhoModel = $sInclude . '/' . $sModulo . '/' . $sModel . '/';
        $sCaminhoPersistencia = $sInclude . '/' . $sModulo . '/' . $sPersistencia . '/';
        $sCaminhoView = $sInclude . '/' . $sModulo . '/' . $sView . '/';

        $sNomeArquivoClasse = montaNomeArquivo($className);
        $sCaminhoArquivo = $sModulo . '_class_' . $sNomeArquivoClasse . '.php';

        if (strpos($className, 'Controller') !== false) {
            $sNovoCaminhoArquivo = $sCaminhoController . $sCaminhoArquivo;
        } else if (strpos($className, 'View') !== false) {
            $sNovoCaminhoArquivo = ($sCaminhoView . $sCaminhoArquivo);
        } else if (strpos($className, 'Persistencia') !== false) {
            $sNovoCaminhoArquivo = ($sCaminhoPersistencia . $sCaminhoArquivo);
        } else if (strpos($className, 'Model') !== false) {
            $sNovoCaminhoArquivo = ($sCaminhoModel . $sCaminhoArquivo);
        } else {
            $sNovoCaminhoArquivo = ('core/' . $sCaminhoArquivo);
        }

        return $sNovoCaminhoArquivo;
    }

    function montaNomeArquivo($sString) {
        $iTamanho = strlen($sString);

        $sLetraDaPosicao = '';
        $sNovaLetra = '';
        $sCaminhoArquivo = '';
        $bVerificaLetra = false;
        for ($i = 0; $i < $iTamanho; $i++) {
            $sLetraDaPosicao = substr($sString, $i - ($iTamanho), 1);
            //echo '<br>Letra da Posicao:'.$i.' - '.$sLetraDaPosicao.'<br>';
            //Verifica se � a primeira ou ultima letra para verificar a maiuscula
            if (($i == 0) || ($i == $iTamanho)) {
                $bVerificaLetra = false;
            } else {
                $bVerificaLetra = true;
            }

            if ($bVerificaLetra) {
                if ($sLetraDaPosicao == (strtoupper($sLetraDaPosicao))) {
                    //Adiciona o Underline->   _            
                    $sNovaLetra = '_' . (strtolower($sLetraDaPosicao));
                } else {
                    $sNovaLetra = $sLetraDaPosicao;
                }
            } else {
                $sNovaLetra = (strtolower($sLetraDaPosicao));
            }
            //Incrementa a letra no caminho
            $sCaminhoArquivo .= $sNovaLetra;
        }
        //echo '<br>novo caminho arquivo:',$sCaminhoArquivo,'<br>';

        return $sCaminhoArquivo;
    }

}
