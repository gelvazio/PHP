<?php

require_once 'funcoes.php';

class Bean {

    public static function getNomeMetodo($iKey) {//Para atributos sem ponto!
        $sString = $iKey;
        $sPrimeiraLetra = substr($sString, 0, strlen($sString) - (strlen($sString) - 1));
        $sRestoLetra = substr($sString, (strlen($sString) - (strlen($sString) - 1)));
        return 'set' . strtoupper($sPrimeiraLetra) . $sRestoLetra;
    }

    public static function callSetter($oModel, $iKey, $xParam) {
        $sMethod = self::getNomeMetodo($iKey);
        callMethod($oModel, $sMethod, $xParam);
    }

    public static function callGetter($oModel, $iKey, $xParam) {
        $sMethod = self::getNomeMetodo($iKey);
        callMethod($oModel, $sMethod, $xParam);
    }

}
