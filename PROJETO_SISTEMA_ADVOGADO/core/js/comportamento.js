function invokeFormulario(sPagina, sAcao, xChave) {
    var oForm = document.createElement('form');
    oForm.setAttribute('action', '?pagina=' + sPagina + '&acao=' + sAcao + '&chave=' + xChave);
    oForm.setAttribute('method', 'POST');
    oForm.submit();
}
/*
 -------------------
 |MODULO CADASTROS |
 -------------------
 */

/*
 ---------------------
 |MODULO FATURAMENTO |
 ---------------------
 */

/*
 --------------------
 |MODULO FINANCEIRO |
 --------------------
 */

/*
 -----------------
 |MODULO COMPRAS |
 -----------------
 */

/*
 --------------------
 |MODULO RELATORIOS |
 --------------------
 */

/*
 ----------------------
 |MODULO CONFIGURACAO |
 ----------------------
 */

function alteraFilial(iCodigo, sNome, sEndereco, sCep, sBairro, sEstado, sMunicipio, sCnpj) {
    document.getElementById('filcodigo').value = iCodigo;
    document.getElementById('filnome').value = sNome;
    document.getElementById('filendereco').value = sEndereco;
    document.getElementById('filcep').value = sCep;
    document.getElementById('filbairro').value = sBairro;
    document.getElementById('filestado').value = sEstado;
    document.getElementById('filmunicipio').value = sMunicipio;
    document.getElementById('filcnpj').value = sCnpj;

}
function excluiFilial(iCodigo) {
    if (window.confirm("Deseja apagar o registro?")) {
        document.getElementById('filcodigo').value = iCodigo;
        document.getElementById('acao').value = 'del';
        document.getElementById('formularioFilial').submit();
    }
}
function validaFormFilial() {
    /*
     var oCodigo = document.getElementById('filfcodigo').value;
     var oNome = document.getElementById('filtitulo').value;
     var oTrailer = document.getElementById('filtrailer').value;
     var oDataEstreia = document.getElementById('fildataestreia').value;
     var oCategoria = document.getElementById('catcodigo').value;
     var oSinopse = document.getElementById('filsinopse').value;
     if (oCodigo === '') {
     alert('Preencha o c�digo!');
     return false;
     } else if (oNome === '') {
     alert('Preencha o nome!');
     return false;
     } else if (oTrailer === '') {
     alert('Preencha o trailer!');
     return false;
     } else if (oDataEstreia === '') {
     alert('Preencha a data de estreia!');
     return false;
     } else if (oCategoria === '') {
     alert('Preencha a categoria!');
     return false;
     } else if (oSinopse === '') {
     alert('Preencha a sinopse!');
     return false;
     } else {
     document.getElementById('formularioFilial').submit();
     }
     */
    document.getElementById('formularioFilial').submit();
}

/*
 ------------------
 |MODULO DE TESTES |
 ------------------
 */
function alteraCategoria(iCodigo, sNome) {
    document.getElementById('catcodigo').value = iCodigo;
    document.getElementById('catnome').value = sNome;

    var labelNome = document.getElementById('idlabel_mensagem');
    labelNome.innerHTML = 'Pressione o botao Gravar para finalizar a operacao de Alteracao!';
}
function excluiCategoria(iCodigo) {
    if (window.confirm("Deseja apagar o registro?")) {
        document.getElementById('catcodigo').value = iCodigo;
        document.getElementById('acao').value = 'del';
        document.getElementById('formularioCategoria').submit();
    }
}
function alteraAtor(iCodigo, sNome) {
    document.getElementById('atocodigo').value = iCodigo;
    document.getElementById('atonome').value = sNome;

    var labelNome = document.getElementById('idlabel_mensagem');
    labelNome.innerHTML = 'Pressione o botao Gravar para finalizar a operacao de Alteracao!';
}
function excluiAtor(iCodigo) {
    if (window.confirm("Deseja apagar o registro?")) {
        document.getElementById('atocodigo').value = iCodigo;
        document.getElementById('acao').value = 'del';
        document.getElementById('formularioAtor').submit();
    }
}
function alteraFilme(iCodigo, sNome, sSinopse, sDataEstreia, sTrailer, iCodigoCategoria, sNomeCategoria) {
    document.getElementById('filcodigo').value = iCodigo;
    document.getElementById('filtitulo').value = sNome;
    document.getElementById('filsinopse').value = sSinopse;
    document.getElementById('fildataestreia').value = sDataEstreia;
    document.getElementById('filtrailer').value = sTrailer;

    var oCodigoCategoria = document.getElementById('catcodigo');
    oCodigoCategoria.value = iCodigoCategoria;
    oCodigoCategoria.options[oCodigoCategoria.selectedIndex].text = sNomeCategoria;

    var labelNome = document.getElementById('idlabel_mensagem');
    labelNome.innerHTML = 'Selecione a Categoria antes de pressionar o botao Gravar!';
}
function excluiFilme(iCodigo) {
    if (window.confirm("Deseja apagar o registro?")) {
        document.getElementById('filcodigo').value = iCodigo;
        document.getElementById('acao').value = 'del';
        document.getElementById('formularioFilme').submit();
    }
}
function alteraFilmeAtor(sPapelAtor, sTituloFilme, sNomeAtor, iCodigoAtor, iCodigoFilme) {
    var oCodigoAtor = document.getElementById('atocodigo');
    oCodigoAtor.value = iCodigoAtor;
    oCodigoAtor.options[oCodigoAtor.selectedIndex].text = sNomeAtor;

    var oCodigoFilme = document.getElementById('filcodigo');
    oCodigoFilme.value = iCodigoFilme;
    oCodigoFilme.options[oCodigoFilme.selectedIndex].text = sTituloFilme;

    document.getElementById('atfpapel').value = sPapelAtor;
    var labelNome = document.getElementById('idlabel_mensagem');
    labelNome.innerHTML = 'Pressionar o botao Gravar para efetuar a operacao!';
}
function excluiFilmeAtor(filmeCodigo, AtorCodigo) {
    if (window.confirm("Deseja apagar o registro?")) {
        document.getElementById('filcodigo').value = filmeCodigo;
        document.getElementById('atocodigo').value = AtorCodigo;
        document.getElementById('acao').value = 'del';
        document.getElementById('formularioFilmeAtor').submit();
    }
}
function validaFormAtor() {
    var oCodigo = document.getElementById('atocodigo').value;
    var oNome = document.getElementById('atonome').value;
    if (oCodigo === '') {
        alert('Preencha o c�digo!');
        return false;
    } else if (oNome === '') {
        alert('Preencha o nome!');
        return false;
    } else {
        document.getElementById('formularioAtor').submit();
    }
}

function validaFormLogin() {
//    var oCodigo = document.getElementById('atocodigo').value;
//    var oNome = document.getElementById('atonome').value;
//    if (oCodigo === '') {
//        alert('Preencha o c�digo!');
//        return false;
//    } else if (oNome === '') {
//        alert('Preencha o nome!');
//        return false;
//    } else {
//        document.getElementById('formularioAtor').submit();
//    }
    document.getElementById('formularioLogin').submit();
}

function validaFormCategoria() {
    var oCodigo = document.getElementById('catcodigo').value;
    var oNome = document.getElementById('catnome').value;
    if (oCodigo === '') {
        alert('Preencha o c�digo!');
        return false;
    } else if (oNome === '') {
        alert('Preencha o nome!');
        return false;
    } else {
        document.getElementById('formularioCategoria').submit();
    }
}
function validaFormFilme() {
    var oCodigo = document.getElementById('filcodigo').value;
    var oNome = document.getElementById('filtitulo').value;
    var oTrailer = document.getElementById('filtrailer').value;
    var oDataEstreia = document.getElementById('fildataestreia').value;
    var oCategoria = document.getElementById('catcodigo').value;
    var oSinopse = document.getElementById('filsinopse').value;
    if (oCodigo === '') {
        alert('Preencha o c�digo!');
        return false;
    } else if (oNome === '') {
        alert('Preencha o nome!');
        return false;
    } else if (oTrailer === '') {
        alert('Preencha o trailer!');
        return false;
    } else if (oDataEstreia === '') {
        alert('Preencha a data de estreia!');
        return false;
    } else if (oCategoria === '') {
        alert('Preencha a categoria!');
        return false;
    } else if (oSinopse === '') {
        alert('Preencha a sinopse!');
        return false;
    } else {
        document.getElementById('formularioFilme').submit();
    }
}
function validaFormFilmeAtor() {
    var oPapel = document.getElementById('atfpapel').value;
    if (oPapel === '') {
        alert('Preencha papel!');
        return false;
    } else {
        document.getElementById('formularioFilmeAtor').submit();
    }
}