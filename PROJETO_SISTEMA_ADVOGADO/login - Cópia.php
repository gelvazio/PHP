<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$oViewManutencaoLogin = new ViewManutencaoLogin();
$oViewManutencaoLogin->montaFormulario();

class ViewManutencaoLogin {

    public function montaFormulario() {
        $sHTML = '<!DOCTYPE html>
        <html lang="pt-br" >
        <head>
          <meta charset="UTF-8">
          <title>Login Form</title>
          <link rel="stylesheet" href="core/css/login.css" />
        </head>
        <body>
          <body>
            <div class="login">
                <div class="login-screen">
                    <div class="app-title">
                        <h1>Login</h1>
                    </div>
                    <div class="login-form">
                        <div class="control-group">
                        <input type="text" class="login-field" value="" placeholder="username" id="login-name">
                        <label class="login-field-icon fui-user" for="login-name"></label>
                    </div>
                    <div class="control-group">
                        <input type="password" class="login-field" value="" placeholder="password" id="login-pass">
                        <label class="login-field-icon fui-lock" for="login-pass"></label>
                    </div>
                        <a class="btn btn-primary btn-large btn-block" href="http://localhost/PROJETO_SISTEMA_ADVOGADO/principal.php">login</a>
                        <a class="login-link" href="#">Lost your password?</a>
                    </div>
                </div>
            </div>
            </body>
            </body>
        </html>';
        echo $sHTML;
    }
    
    public function montaTela() {
        $this->montaFormulario();
    }

}
