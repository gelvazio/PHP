<?php

class ModelUsuario extends ModelPadrao {

    private $cd_usuario;
    private $ds_login;
    private $ds_usuario;
    private $ds_senha;
    private $ModelGrupoUsuario;
    private $ModelFilial;
    private $fg_ativo;

    function getCd_usuario() {
        return $this->cd_usuario;
    }

    function getDs_login() {
        return $this->ds_login;
    }

    function getDs_usuario() {
        return $this->ds_usuario;
    }

    function getDs_senha() {
        return $this->ds_senha;
    }

    function getModelGrupoUsuario() {
        return $this->ModelGrupoUsuario;
    }

    function getModelFilial() {
        return $this->ModelFilial;
    }

    function getFg_ativo() {
        return $this->fg_ativo;
    }

    function setCd_usuario($cd_usuario) {
        $this->cd_usuario = $cd_usuario;
    }

    function setDs_login($ds_login) {
        $this->ds_login = $ds_login;
    }

    function setDs_usuario($ds_usuario) {
        $this->ds_usuario = $ds_usuario;
    }

    function setDs_senha($ds_senha) {
        $this->ds_senha = $ds_senha;
    }

    function setModelGrupoUsuario($ModelGrupoUsuario) {
        $this->ModelGrupoUsuario = $ModelGrupoUsuario;
    }

    function setModelFilial($ModelFilial) {
        $this->ModelFilial = $ModelFilial;
    }

    function setFg_ativo($fg_ativo) {
        $this->fg_ativo = $fg_ativo;
    }

}
