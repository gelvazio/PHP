<?php

class ModelGrupoUsuario extends ModelPadrao {

    private $cd_grupo;
    private $ds_grupo;
    private $ModelFilial;

    function getCd_grupo() {
        return $this->cd_grupo;
    }

    function getDs_grupo() {
        return $this->ds_grupo;
    }

    function getModelFilial() {
        return $this->ModelFilial;
    }

    function setCd_grupo($cd_grupo) {
        $this->cd_grupo = $cd_grupo;
    }

    function setDs_grupo($ds_grupo) {
        $this->ds_grupo = $ds_grupo;
    }

    function setModelFilial($ModelFilial) {
        $this->ModelFilial = $ModelFilial;
    }

}
