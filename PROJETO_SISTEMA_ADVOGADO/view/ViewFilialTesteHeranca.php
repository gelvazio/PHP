<?php

class ViewFilialTesteHeranca extends ViewFilial {

    /**
     *
     * @var ModelPadrao
     */
    private $Model;

    public function montaFormulario() {
        parent::montaFormulario();
        $this->Model->setNomeClasse("FilialTesteHeranca");
        echo ('Nome da classe:' + $this->Model->getNomeClasse());
    }

}
