<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ViewManutencaoLogin extends ViewManutencaoPadrao {

    /**
     * @var ModelFilial
     */
    protected $Model;

    public function montaCabecalho() {
        //parent::montaCabecalho();
    }

    public function montaTitulo() {
        $sHTML = '';
        $sHTML .= '<h4><p>Login de Acesso</p></h4>';
        echo $sHTML;
    }

    public function montaConsulta() {
        $sHTML = '';
        $sHTML .= '<hr>';
        $sHTML .= '<hr>';
        $sHTML .= '<h4><p>Lista de Filiais cadastradas</p></h4>';
        $sHTML .= '<table class="tabela_consulta">';
        $sHTML .= '<tr>';
        $sHTML .= '<th>C�digo</th>';
        $sHTML .= '<th>Nome</th>';
        $sHTML .= '<th>Endere�o</th>';
        $sHTML .= '<th>Cep</th>';
        $sHTML .= '<th>Bairro</th>';
        $sHTML .= '<th>Estado</th>';
        $sHTML .= '<th>Municipio</th>';
        $sHTML .= '<th>CNPJ</th>';
        $sHTML .= '<th colspan="2">A��es</th>';
        $sHTML .= '</tr>';
        if (count($this->listaModel) > 0) {
            foreach ($this->listaModel as $indice => /* @var $oModelFilial ModelFilial */ $oModelFilial) {
                $sHTML .= '<tr>';
                $sHTML .= '<td><input id="botaoSelecionar" type="checkbox" /></td>';
                $sHTML .= '<td>' . $oModelFilial->getFilcodigo() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilnome() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilendereco() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilcep() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilbairro() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilestado() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilmunicipio() . '</td>';
                $sHTML .= '<td>' . $oModelFilial->getFilcnpj() . '</td>';
                $sHTML .= '<td>';
                $sHTML .= '<a href="#"><input class="button" id="botaoAlterar" type="button" value="Alterar" onclick="alteraFilial('
                        . '  ' . $oModelFilial->getFilcodigo() . ','
                        . '\'' . $oModelFilial->getFilnome() . '\','
                        . '\'' . $oModelFilial->getFilendereco() . '\','
                        . '\'' . $oModelFilial->getFilcep() . '\','
                        . '\'' . $oModelFilial->getFilbairro() . '\','
                        . '\'' . $oModelFilial->getFilestado() . '\','
                        . '\'' . $oModelFilial->getFilmunicipio() . '\','
                        . '\'' . $oModelFilial->getFilcnpj() . '\''
                        . ')"/></a>';
                $sHTML .= '<input class="button" id="botaoExcluir" type="button" value="Excluir" onclick="excluiFilial(' . $oModelFilial->getFilcodigo() . ')"/>';
                $sHTML .= '</td>';
                $sHTML .= '</tr>';
            }
        } else {
            $sHTML .= '<tr><td colspan="9" >Nenhuma Filial Cadastrada</td></tr>';
        }
        $sHTML .= '</table>';
        $sHTML .= '<hr>';
        echo $sHTML;
    }

    public function montaFormulario($bLoginInvalido = false) {
        //$sHTML .= '<form method="POST" action="index.php?pagina="Filial" id="formularioFilial">';
        
        
        $sLogin = $bLoginInvalido ? 'Login Inv�lido!Tente Novamente!' : 'Login';
        $sHTML = '<!DOCTYPE html>
        <html lang="pt-br" >
        <head>
          <meta charset="UTF-8">
          <title>Login Form</title>
          <link rel="stylesheet" href="core/css/login.css" />
        </head>
        <body>
          <body>
                <div class="login">
                    <form method="POST" action="index.php?pagina="ManutencaoLogin" id="formularioLogin">
                    <div class="login-screen">
                        <div class="app-title">
                            <h1>' .$sLogin. '</h1>
                        </div>
                        <div class="login-form">
                            <div class="control-group">
                            <input type="text" class="login-field" value="" placeholder="username" id="login" name="login">
                            <label class="login-field-icon fui-user" for="login-name"></label>
                        </div>
                        <div class="control-group">
                            <input type="password" class="login-field" value="" placeholder="password" id="senha" name="senha">
                            <label class="login-field-icon fui-lock" for="login-pass"></label>
                        </div>
                            <a class="btn btn-primary btn-large btn-block" href="#">login</a>
                            <a class="login-link" href="#">Lost your password?</a>
                        </div>
                    </div>                
                    </form>
                </div>                
            </body>
        </html>';
        echo $sHTML;
    }
    
    public function montaTela() {
        $this->montaFormulario();
    }

}
