<?php

/* Controller Global da Estrutura
 * @package GLW
 * @subpackage View
 * @author Gelvazio Camargo
 * @since 19/07/2016
 */
//require_once('../persistencia/glw_class_persistencia_manutencao_padrao.inc');

class ControllerManutencaoPadrao {

    CONST ACAO           = 'acao';
    CONST ACAO_INCLUIR   = 'inc';
    CONST ACAO_ALTERAR   = 'alt';
    CONST ACAO_EXCLUIR   = 'exc';
    CONST ACAO_CONSULTAR = 'con';
    
    protected $Persistencia;
    
    public function __construct() {
        $this->Persistencia = $this->getInstancePersistencia();
    }
    
    public function processaDados() {
        if (Redirecionador::getParametro(self::ACAO)) {           
            $this->Persistencia->setModel($this->setModelFromView());
            switch (Redirecionador::getParametro(self::ACAO)) {
                case self::ACAO_INCLUIR:
                    $this->processaDadosInclusao();
                    break;
                case self::ACAO_EXCLUIR:
                    $this->processaDadosExclusao();
                    break;
                case self::ACAO_ALTERAR:
                    $this->processaDadosAlteracao();
                    break;
                case self::ACAO_CONSULTAR:
                    $this->processaDadosSemTela();
                    break;
            }
        }
        $this->montaTela();
    }

    protected function setModelFromView() {
        $oModeloFilial = new ModelFilial();
        $oModeloFilial->setFilcodigo(Redirecionador::getParametro('filcodigo'));
        $oModeloFilial->setFilnome(Redirecionador::getParametro('filnome'));
        $oModeloFilial->setFilendereco(Redirecionador::getParametro('filendereco'));
        $oModeloFilial->setFilcep(Redirecionador::getParametro('filcep'));
        $oModeloFilial->setFilbairro(Redirecionador::getParametro('filbairro'));
        $oModeloFilial->setFilestado(Redirecionador::getParametro('filestado'));
        $oModeloFilial->setFilmunicipio(Redirecionador::getParametro('filmunicipio'));
        $oModeloFilial->setFilcnpj(Redirecionador::getParametro('filcnpj'));

        return $oModeloFilial;
    }

    protected function processaDadosInclusao() {
        $this->getInstancePersistencia()->setModel($this->setModelFromView());
        $this->getInstancePersistencia()->insere();
    }

    protected function processaDadosAlteracao() {
        $this->getInstancePersistencia()->setModel($this->setModelFromView());
        $this->getInstancePersistencia()->altera();
    }

    protected function processaDadosExclusao() {
        $this->getInstancePersistencia()->setModel($this->setModelFromView());
        $this->getInstancePersistencia()->exclui();
    }

    protected function processaDadosSemTela() {
        $this->getInstancePersistencia()->setModel($this->setModelFromView());
        $this->getInstancePersistencia()->consulta();
    }

    protected function montaTela() {
        $oViewManutencaoPadrao = $this->getInstanceViewManutencaoPadrao();
        $oViewManutencaoPadrao->setListaModel($this->getInstancePersistencia()->getAll());
        $oViewManutencaoPadrao->montaTela();
    }

    /**
     * Retorna a persistencia Padrao
     * @static var PersistenciaManutencaoPadrao $oPersistenciaManutencaoPadrao
     * @return \PersistenciaManutencaoPadrao
     */
    protected function getInstancePersistencia() {
        throw new Exception('Persistencia Padr�o n�o informada!');
    }
    
    protected function getInstanceViewManutencaoPadrao() {
        static $oViewPadrao;
        if (!isset($oViewPadrao)) {
            $oViewPadrao = new ViewManutencaoPadrao();
        }
        return $oViewPadrao;
    }

}
