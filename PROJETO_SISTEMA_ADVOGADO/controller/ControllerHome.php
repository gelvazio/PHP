<?php

class ControllerHome extends ControllerManutencaoPadrao {

    public function processaDados() {
        $this->MontaTela();
    }

    public function validaLogin() {
        $sLogin = Redirecionador::getParametro('login');
        $sSenha = Redirecionador::getParametro('senha');
        //return $this->getInstancePersistencia()->validaLogin($sLogin, $sSenha);
        return  false;
    }
    
    public function MontaTela() {
        if($this->validaLogin()){
            $oViewHome = new ViewHome();
            $oViewHome->montaTela();            
        } else {
            $oViewManutencaoLogin = new ViewManutencaoLogin();
            $oViewManutencaoLogin->montaFormulario($bLoginInvalido = true); 
        }
    }

    protected function getInstancePersistencia() {
        return new PersistenciaLogin();
    }
}
