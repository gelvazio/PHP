<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ControllerManutencaoLogin extends ControllerManutencaoPadrao {
    
    protected function getInstancePersistencia() {
        return new PersistenciaLogin();
    }
    
    protected function getInstanceViewManutencaoPadrao() {
        return new ViewManutencaoLogin();
    }
    
    public function validaLogin() {
        $sLogin = Redirecionador::getParametro('login');
        $sSenha = Redirecionador::getParametro('senha');
        return $this->getInstancePersistencia()->validaLogin($sLogin, $sSenha);
    }
    
    public function montaTela() {
        if($this->validaLogin()){
            $oViewHome = new ViewHome();
            $oViewHome->montaTela();            
        } else {
            $oViewManutencaoLogin = new ViewManutencaoLogin();
            $oViewManutencaoLogin->montaFormulario($bLoginInvalido = true); 
        }
    }
}

