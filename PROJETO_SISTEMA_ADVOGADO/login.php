<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$oViewManutencaoLogin = new ViewManutencaoLogin();
$oViewManutencaoLogin->montaFormulario();

class ViewManutencaoLogin {

    public function montaFormulario() {
        $sHTML = '<!DOCTYPE html>
        <html lang="pt-br" >
        <head>
          <meta charset="UTF-8">
          <title>Login Form</title>
          <link rel="stylesheet" href="core/css/login.css" />
          <script src="core/assets/js/comportamento.js"></script>
        </head>
        <body>
          <body>
            <div class="login">
                <div class="login-screen">
                    <div class="app-title">
                        <h1>Login</h1>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="index.php?pagina="ManutencaoLogin" id="formularioLogin">
                            <div class="control-group">
                                <input type="text" class="login-field" value="" placeholder="username" id="login">
                                <label class="login-field-icon fui-user" for="login-name"></label>
                            </div>
                            <div class="control-group">
                                <input type="password" class="login-field" value="" placeholder="password" id="senha">
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>
                            <a class="btn btn-primary btn-large btn-block" href="principal.php">login</a>
                            
                    <div class="botaoAcao">
                        <input class="btn btn-primary btn-large btn-block" type="button" value="Entrar" onclick="validaFormLogin()"/>
                        <input type="reset" value="Limpar"/>
                            <a class="login-link" href="#">Lost your password?</a>
                    </div>
                    
                        </form>
                    </div>
                </div>
            </div>
            </body>
            </body>
        </html>';
        echo $sHTML;
    }
    
    public function montaTela() {
        $this->montaFormulario();
    }

}
