<?php
 
class RequisicaoHttp {
	
	/**
     * Codigo do metodo GET
     */
    const METHOD_GET = 80;

    /**
     * Codigo do metodo POST
     */
    const METHOD_POST = 47;
 
    private $Http;
    private $Retorno;
    private $Header;
    
    public function __construct($sURL = false, $sMetodo = 'GET', $bUseProxy = false){
        //parametros iniciais
        $this->Http = curl_init();
        $this->setUrl($sURL);
        $this->setMetodo($sMetodo);

        //seta opcoes
        $this->setDefaultOptions();

        if ($bUseProxy) {
            $aInfo[CURLOPT_PROXYAUTH]    = CURLAUTH_BASIC;
            $aInfo[CURLOPT_PROXYTYPE]    = CURLPROXY_HTTP;
            $aInfo[CURLOPT_PROXY]        = 'proxy.ipm.com.br:3128';
            $aInfo[CURLOPT_PROXYUSERPWD] = 'acesso.publico:IPM@1234567';
            $this->setOptions($aInfo);
        }
    }

    public function send(){
        $sResponse     = curl_exec($this->Http);
        $iHeaderSize   = curl_getinfo($this->Http, CURLINFO_HEADER_SIZE);
        $this->Retorno = trim(substr($sResponse, $iHeaderSize, strlen($sResponse)));
        $this->Header  = trim(substr($sResponse, 0, $iHeaderSize));
        unset($sResponse);
    }

    public function setCorpo($corpo){
        curl_setopt($this->Http, CURLOPT_POSTFIELDS, $corpo);
    }
    
    public function &getCorpo(){
        return $this->Retorno;
    }    
	
	/**
     * Seta o endereco da requisicao
     * @param String $sUrl - EndereÃ§o para aonde a requisiÃ§Ã£o serÃ¡ feita
     */
    public function setUrl($sUrl){
        curl_setopt($this->Http, CURLOPT_URL, $sUrl);
    }
	
	/**
     * Seta o tipo do meodo da requisicao podendo ser GET e POST
     * @param String $sMetodo - Nome do tipo do metodo (GET,POST)
     */
    public function setMetodo($sMetodo){
        if(is_string($sMetodo)){
            $iMethod = constant('RequisicaoHttp::METHOD_' . strtoupper($sMetodo));
        }
        else{
            $iMethod = $sMetodo;
        }
        curl_setopt($this->Http, $iMethod, true);
    }

    /**
     * Seta configuracoes padrao da requisicao
     */
    private function setDefaultOptions(){
        $this->setOptions(Array(
             CURLOPT_HEADER         => true
            ,CURLOPT_RETURNTRANSFER => true
            ,CURLOPT_VERBOSE        => true
            ,CURLOPT_SSL_VERIFYPEER => false
            ,CURLOPT_VERBOSE        => false
        ));
    }
		
    /**
     * Permite setar demais operacoes para a requisicao
     * @param Array $aOptions - Array de opcoes
     */
    public function setOptions(Array $aOptions){
        curl_setopt_array($this->Http, $aOptions);
    }
	
}

class BuscaDadosSTF {

    public function getDadosRespostaCabecalho(){
        //return file_get_contents('C:/ipm/htdocs/projeto_stf/temp/resposta_cabecalho_inicial.html');
		$sLink = 'http://www.stf.jus.br/portal/processo/listarProcessoUnico.asp';
	
        $sNumeroProcesso = '50003286620104047008'; //sem pontos
        $oRequisicaoHttp = new RequisicaoHttp($sLink, 'POST', false);
        
        $aParams = array('numero' => $sNumeroProcesso, 'partesAdvogadosRadio' => 1, 'dropmsgoption' => 5);
        $oRequisicaoHttp->setCorpo(http_build_query($aParams));
        $oRequisicaoHttp->setOptions(Array(CURLOPT_FOLLOWLOCATION => true));
        
        $oRequisicaoHttp->send();
        $sHTML = $oRequisicaoHttp->getCorpo();
        file_put_contents('C:/Mega/MANUAIS_PROJETOS_XAMPP/www/temp_stf/resposta_cabecalho.html', $sHTML);
        return $sHTML;        
    }
    
    public function processaDados(){        
        $sHTML = $this->getDadosRespostaCabecalho();
        //vamos criar um DOM apenas para buscar o numero de incidencia para prosseguir para a proxima etapa
	$oDom = new DOMDocument('1.0', 'UTF-8');
	@$oDom->loadHTML($sHTML);
	$childNodeList = $oDom->getElementsByTagName('table');
        $childNodeList;
	foreach($childNodeList as $oChild) {
	    $aPaginaToGetLink = $this->domElement2array($oChild);
	    $sLink            = $aPaginaToGetLink['tr1']['td']['a']['href'];	    
	    $aTemp            = explode('=', $sLink);
	    $iIncidencia      = $aTemp[1];	    
	    return $this->buscaMovimentos($iIncidencia);
	}
    }

    public function buscaMovimentos($iIncidente){       
        //$sLink = 'http://www.stf.jus.br/portal/processo/verProcessoAndamento.asp?incidente='.$iIncidente;
        $sLink = 'http://portal.stf.jus.br/processos/detalhe.asp?incidente='.$iIncidente;
        $oRequisicaoHttp = new RequisicaoHttp($sLink, 'GET', false);
        $oRequisicaoHttp->setOptions(Array(CURLOPT_FOLLOWLOCATION => true));
        $oRequisicaoHttp->send();
        $sHTML = $oRequisicaoHttp->getCorpo();
        file_put_contents('C:/ipm/htdocs/projeto_stf/temp/resposta_movimento_stf.html', $sHTML);
        return $sHTML;
    }

    private static function superTrim($sValor, $bAnySpace = false, $bMultiline = true) {
        if($bAnySpace){
            //qualquer espaco e quebra de linha
            $regExp = '/\s+/';
        } else {
            $regExp = '/\h+/';
        }
        return preg_replace(Array('/' . ($bMultiline ? '(?m)' : '') . '(^\h*)|(\h*$)/', $regExp), array('',' '), $sValor);
    }
    
    private function domElement2array(DomElement $xmlObject) {
        $out = Array();
	$out[$xmlObject->nodeName] = utf8_decode(self::superTrim($xmlObject->nodeValue, true, true));

        foreach($xmlObject->attributes as $oItem) {
            $out[$oItem->nodeName] = utf8_decode(self::superTrim($oItem->nodeValue, true, true));
        }
        foreach ($xmlObject->childNodes as $i => $filhos) {
            if ($filhos instanceof DOMElement) {
		$sCurrentNodeName = $this->getCurrentNodeName($out, $filhos->nodeName);
		$out[$sCurrentNodeName] = $this->domElement2array($filhos);
            }
        }
        return $out;
    }
    
    private function getCurrentNodeName($aArrayNode, $sNodeName) {
        $sNewNodeName = false;
	while(!$sNewNodeName) {
	    if (!isset($aArrayNode[$sNodeName])) {
		$sNewNodeName = $sNodeName;
	    } else {
		$aChavesArray = array_keys($aArrayNode);
		$iCount = 0;
		foreach($aChavesArray as $sChave) {
		    if(strpos($sChave, $sNodeName) !== false) {
			$iCount++;
		    }
		}		
		$sNewNodeName = $sNodeName . $iCount;		
	    }
	}	
	return $sNewNodeName;
    }
    
}

$oBuscaDados = new BuscaDadosSTF();
$oBuscaDados->processaDados();
